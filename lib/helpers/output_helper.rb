module Mentor

  module OutputHelper

    def a_an(word)
      %w(A E I O U).include?(word[0]) ? 'an' : 'a'
    end

    def culprit_line
      lines_from_file[tp.lineno]
    end

    def pluralize(word)
      word.to_s + (word.to_s == 'Hash' ? 'es' : 's')
    end

    def pluralize_words(words)
      words.map { |word| pluralize word }
    end

    def or_sentence(words)
      words.join(' or ')
    end

    def and_sentence(words)
      words.join(' and ')
    end

    def home_to_tilde(path)
      path.sub(ENV['HOME'], '~')
    end

    def indent_lines(*lines, indent: 2)
      lines.flatten!

      indent.downto(1).each do |number_of_spaces|
        if lines.all? { |line| line.size + indent * 2 <= terminal_width }
          return lines.map { |line| ' ' * number_of_spaces + line }
        end
      end

      lines
    end

    def lines_from_file
      return @lines_from_file if @lines_from_file
      file = File.new(Mentor.tp.path)
      @lines_from_file = file.map { |line| [file.lineno, line.chomp] }.to_h
    end

    def terminal_width
      `tput cols`.to_i
    end

    def valid_var_name
      /(@@|@|\$)?[a-zA-Z][a-zA-Z_0-9]*/
    end

  end

end
