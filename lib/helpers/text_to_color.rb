module Mentor

  class TextToColor

    include Colorize

    def initialize(full_text, text_to_color, output_type, match_pattern = nil)
      @full_text     = full_text
      @text_to_color = text_to_color
      @output_type   = output_type
      @match_pattern = match_pattern
    end

    def color_pattern
      @full_text.gsub(@match_pattern, colored)
    end

    def colored
      return @full_text unless text_to_color_exists?
      color_text
    end

    private

    def colorized_text
      rainbow(@text_to_color, @output_type)
    end

    def text_to_color_exists?
      @text_to_color && !@text_to_color.empty?
    end

    def color_text
      remaining_text = @match_pattern || @full_text
      text_processed = ''

      while remaining_text[@text_to_color]
        before_text                  = before_text_to_color(remaining_text)
        reset_color                  = get_reset_color(before_text).to_s
        text_processed              += before_text + colorized_text
        index_after_text_processed   = (before_text + @text_to_color).size
        remaining_text               = reset_color + remaining_text[index_after_text_processed..-1]
      end

      text_processed + remaining_text
    end

    def before_text_to_color(remaining_text)
      index = remaining_text.index(@text_to_color)
      return '' if !index || index.zero?
      remaining_text[0..index - 1]
    end

    def get_reset_color(before_text)
      index_of_color_to_reset_to = before_text.rindex(color_regex)
      before_text[index_of_color_to_reset_to..-1][color_regex] if index_of_color_to_reset_to
    end

    def color_regex
      /\e\[(\d|;)+m/
    end

  end

end
