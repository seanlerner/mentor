module Mentor

  dir = __dir__ + '/../..'

  gems = %w[pry rainbow rouge]

  helpers = %w[
    output_helper
    outputs
    colorize
    text_to_color
  ].map { |file| dir + '/lib/helpers/' + file }

  sections = Dir.glob(dir + '/lib/sections/*.rb').sort
  errors   = Dir.glob(dir + '/lib/errors/*.rb').sort

  (gems + helpers + sections + errors).each { |file| require file }

  require_relative '../main'

end
