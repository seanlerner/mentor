module Mentor

  module Outputs

    include OutputHelper

    private

    def tp
      Mentor.tp
    end

    def raised_exception
      tp.raised_exception
    end

    def absolute_base_dir
      Dir.pwd + '/'
    end

    def app_dir
      tp.path                       # Full path
        .sub(absolute_base_dir, '') # Remove path up until this file
        .split('/')[0..-2]          # Split path into parts and exclude filename
        .join('/') +                # Join with '/'
        '/'                         # Add one more '/' to end of it
    end

    def backtrace_lines

      # raised_exception.backtrace disappears after accessing it once
      # So we just do it once and then save the result to @@backtrace_lines
      if self.class.class_variable_defined?(:@@backtrace_lines)
        return @@backtrace_lines
      end

      bt_lines = raised_exception
                 .backtrace[1..-1]                            # Remove first
                 .grep_v(%r{bin/mentor})                       # Remove mentor involvement
                 .map { |line| line.sub(Dir.pwd + '/', '') }  # Remove path before current dir
                 .map { |line| 'from ' + line }               # Add 'from'

      return bt_lines if bt_lines.empty?

      # Make it appear so backtrace came from `<main>' instead of being required by mentor
      bt_lines[-1].gsub!("`<top (required)>'", "`<main>'")

      @@backtrace_lines = bt_lines

    end

    def calling_method
      (tp.method_id || '<main>').to_s
    end

    def did_you_mean_word
      return unless raised_exception.respond_to? :corrections

      raised_exception.corrections.first.to_s
    end

    def did_you_mean_text
      'Did you mean?'
    end

    def error_lineno_padded(width)
      "=> #{@lineno}".rjust(width)
    end

    def error_lineno
      tp.lineno.to_s
    end

    def file_name
      tp.path.split('/').last
    end

    def horizontal_line
      '─' * terminal_width
    end

    def lineno_subtle_padded(width)
      @lineno.to_s.rjust(width)
    end

    def literal_class
      tp.raised_exception.receiver.class.to_s
    end

    def literal_for_method
      tp.raised_exception.receiver.to_s
    end

    def quoted_literal_for_method
      quote = culprit_line[culprit_line.index(tp.raised_exception.receiver) - 1]
      quote + tp.raised_exception.receiver + quote
    end

    def message
      if raised_exception.respond_to? :original_message
        raised_exception.original_message
      else
        raised_exception.message
      end
    end

    def method_name
      return unless
        raised_exception.respond_to?(:spell_checker) &&
        raised_exception.spell_checker.respond_to?(:method_name)

      raised_exception.spell_checker.method_name.to_s
    end

    def relative_base_dir
      home_to_tilde(absolute_base_dir)
    end

    def ruby_error_class
      raised_exception.class.to_s
    end

    def ruby_error_text
      'Ruby Error:'
    end

    def var_for_method
      var = culprit_line[/#{valid_var_name}\.#{method_name}/].to_s.chomp(".#{method_name}")
      if var.empty?
        culprit_line[/#{valid_var_name} method_name/].to_s.chomp(".#{method_name}")
      end
      var
    end

  end

end
