module Mentor

  module Colorize

    include OutputHelper

    def colorize(line)

      if line_of_padded_code?(line)
        line = color_padded_code(line)
      elsif backtrace?(line)
        return rainbow(line, :backtrace_line)
      end

      apply_colors(line)
    end

    def colorize_section
      @lines.map! { |line| colorize(line) }
    end

    private

    def backtrace?(line)
      line['from ']
    end

    def line_of_padded_code?(line)
      line[padded_lineno_with_colon_regex]
    end

    def output_types
      %i[
        did_you_mean_text   did_you_mean_word
        message             horizontal_line            ruby_error_text
        calling_method      method_name                var_for_method
        absolute_base_dir   relative_base_dir          app_dir  file_name
      ]
    end

    def apply_colors(line)
      line = apply_literal_for_method(line) if did_you_mean_with_literal_error?
      line = apply_pattern_colors(line)
      line = apply_output_colors(line)
      line = apply_nil_colors(line)
      line = apply_common_classes_colors(line)
    end

    def apply_literal_for_method(line)
      line = TextToColor.new(line, literal_for_method, :literal_for_method, "#{literal_for_method} is a").color_pattern
      line = TextToColor.new(line, literal_for_method, :literal_for_method, "for #{literal_for_method}:").color_pattern
      line = TextToColor.new(line, literal_for_method, :literal_for_method, %Q{"#{literal_for_method}"}).color_pattern
      line = TextToColor.new(line, literal_for_method, :literal_for_method, "'#{literal_for_method}'").color_pattern
    end

    def apply_pattern_colors(line)
      line = TextToColor.new(line, error_lineno,       :error_lineno,       "#{file_name}:#{error_lineno}").color_pattern
      line = TextToColor.new(line, ruby_error_class,   :ruby_error_class,   "(#{ruby_error_class})").color_pattern
      line = TextToColor.new(line, ruby_error_class,   :ruby_error_class,   " #{ruby_error_class} ").color_pattern
    end

    def apply_output_colors(line)
      output_types.reduce(line) do |new_line, output_type|
        TextToColor.new(new_line, send(output_type), output_type).colored
      end
    end

    def apply_nil_colors(line)
      line = TextToColor.new(line, ' nil', :nil_text).colored
      line = TextToColor.new(line, 'NilClass', :nil_text).colored
    end

    def apply_common_classes_colors(line)
      %w(String Integer Float Array Hash [] {}).reduce(line) do |new_line, klass|
        new_line = TextToColor.new(new_line, pluralize(klass), :common_class).colored
        new_line = TextToColor.new(new_line, klass.to_s, :common_class).colored
      end
    end

    def color_padded_code(line)
      padded_lineno = line[padded_lineno_with_colon_regex]
      code          = line[padded_lineno.size..-1]
      color_padded_lineno(padded_lineno) + color_code(code)
    end

    def color_code(code)
      if did_you_mean_with_literal_error?
        TextToColor.new(code, literal_for_method, :literal_for_method).colored
      else
        formatter = Rouge::Formatters::Terminal256.new
        lexer     = Rouge::Lexers::Ruby.new
        formatter.format(lexer.lex(code))
      end
    end

    def color_padded_lineno(padded_lineno)
      text_to_color_text = padded_lineno[padded_lineno_regex]
      color              = text_to_color_text['=>'] ? :error_lineno : :subtle
      TextToColor.new(padded_lineno, text_to_color_text, color).colored
    end

    def padded_lineno_regex
      /^\s+(=>)?\s\d+/
    end

    def padded_lineno_with_colon_regex
      /^\s+(=>)?\s\d+:/
    end

    def rainbow(str, output_type)
      Rainbow(str).color(colors[output_type])
    end

    def did_you_mean_with_literal_error?
      Mentor.tp.raised_exception.respond_to?(:corrections) &&
      Mentor.tp.raised_exception.corrections.any? &&
      [String, Integer, Float].include?(Mentor.tp.raised_exception.receiver.class)
    end

    def colors
      error        = :tomato
      error_lineno = :gold
      method_name  = :deepskyblue
      subtle       = :olive
      very_subtle  = :dimgray
      nil_text     = :orange
      literal      = :mediumpurple

      {
        app_dir:                   subtle,
        backtrace_line:            very_subtle,
        absolute_base_dir:         subtle,
        relative_base_dir:         subtle,
        calling_method:            :green,
        common_class:              :cornflower,
        did_you_mean_text:         :royalblue,
        did_you_mean_word:         method_name,
        error_lineno:              error_lineno,
        error_lineno_padded:       error_lineno,
        file_name:                 :greenyellow,
        horizontal_line:           :red,
        lineno_subtle_padded:      subtle,
        message:                   error,
        method_class:              :mediumpurple,
        method_name:               method_name,
        nil_text:                  nil_text,
        other_class_or_module:     :darksalmon,
        prominent:                 :ivory,
        ruby_error_class:          :orangered,
        literal_for_method:        literal,
        quoted_literal_for_method: literal,
        ruby_error_text:           error,
        subtle:                    subtle,
        suggestion:                :lightgreen,
        var_for_method:            :palevioletred
      }
    end

  end

end
