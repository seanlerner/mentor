module Mentor

  module Globals

    attr_accessor :tp

    def enable
      @enabled = true
    end

    def disable
      @enabled = false
    end

    def enabled?
      @enabled == true
    end

  end

end
