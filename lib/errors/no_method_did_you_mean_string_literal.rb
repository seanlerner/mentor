module Mentor

  class NoMethodDidYouMeanStringLiteralError < MentorNoMethodError

    def self.can_handle?
      super &&
        Mentor.tp.raised_exception.respond_to?(:corrections) &&
        Mentor.tp.raised_exception.corrections.any? &&
        var_for_method.empty? &&
        Mentor.tp.raised_exception.receiver.is_a?(String)
    end

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new,
        ErrorClassSpecificHelp.new(error_class_specific_help),
        Suggestion.new("Try changing the method #{method_name} to #{did_you_mean_word} on your #{literal_class}.")
      ]
    end

    private

    def error_class_specific_help
      [
        "#{quoted_literal_for_method} is #{a_an(literal_class)} #{literal_class}.",
        '',
        "#{pluralize(literal_class)} do not have the method #{method_name}.",
        '',
        'You may have made a typo.'
      ]
    end

  end

end
