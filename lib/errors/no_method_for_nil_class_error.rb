module Mentor

  class NoMethodForNilClassError < MentorNoMethodError

    def self.can_handle?
      super && Mentor.tp.raised_exception.to_s['NilClass']
    end

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new,
        ErrorClassSpecificHelp.new([error_class_specific_help]),
        Suggestion.new("Try setting #{var_for_method} to a value first and then call the method #{method_name} on it.")
      ]
    end

    private

    def error_class_specific_help
      [
        "It looks like you're trying to call the method #{method_name} on #{var_for_method}.",
        '',
        "It's likely #{var_for_method} was never defined, and therefore is equal to nil, which is how Ruby represents 'nothing'."
      ]
    end

  end

end



