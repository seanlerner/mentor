module Mentor

  class MentorError < StandardError

    include Outputs, Colorize

    def self.find
      error_classes.find(&:can_handle?)
    end

    def self.can_handle?
      Mentor.tp.raised_exception.class == RuntimeError
    end

    def output
      puts
      sections_formatted.each do |section|
        puts section
        puts
      end
    end

    def self.error_classes
      [
        NoMethodDidYouMeanStringLiteralError,
        NoMethodDidYouMeanNumericLiteralError,
        NoMethodDidYouMeanSuggestionError,
        NoMethodForNilClassForCommonClassError,
        NoMethodForNilClassError,
        MentorNoMethodError,
        MentorError
      ]
    end

    private

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new
      ]
    end

    def sections_formatted
      sections.map do |section|
        section.lines.map do |line|
          colorize line
        end
      end
    end

  end

end
