module Mentor

  class NoMethodDidYouMeanSuggestionError < MentorNoMethodError

    def self.can_handle?
      super &&
        Mentor.tp.raised_exception.respond_to?(:corrections) &&
        Mentor.tp.raised_exception.corrections.any?
    end

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new,
        ErrorClassSpecificHelp.new(error_class_specific_help),
        Suggestion.new("Try changing the method #{method_name} to #{did_you_mean_word} on #{var_for_method}.")
      ]
    end

    private

    def error_class_specific_help
      [
        "#{var_for_method} does not have the method #{method_name}.",
        '',
        'You may have made a typo.'
      ]
    end

  end

end
