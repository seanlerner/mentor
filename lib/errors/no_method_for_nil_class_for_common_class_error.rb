module Mentor

  class NoMethodForNilClassForCommonClassError < MentorNoMethodError

    def self.can_handle?
      super &&
        Mentor.tp.raised_exception.to_s['NilClass'] &&
        common_methods.include?(method_name.to_sym)
    end

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new,
        ErrorClassSpecificHelp.new(error_class_specific_help),
        Examples.new(examples),
        Docs.new(docs),
        Suggestion.new("Try setting #{var_for_method} to #{a_an(common_classes_for_error.keys.first)} #{or_sentence(common_classes_for_error.keys)} first and then call the method #{method_name} on it.")
      ]
    end

    private

    def error_class_specific_help
      [
        "It looks like you're trying to call the method #{method_name} on #{var_for_method}.",
        '',
        "It's likely #{var_for_method} was never defined, and therefore is equal to nil, which is how Ruby represents 'nothing'.",
        '',
        "#{and_sentence(pluralize_words(common_classes_for_error.keys))} have the #{method_name} method, so you may want to set #{var_for_method} to #{a_an(common_classes_for_error.keys.first)} #{or_sentence(common_classes_for_error.keys)} first.",
      ]
    end

    def examples
      common_classes_for_error.map { |klass, attrs| "  #{var_for_method} = #{attrs[:example]}  # sets it to #{a_an(klass)} #{klass}" }
    end

    def docs
      common_classes_for_error.keys.map { |klass| "ri #{klass}##{method_name}" }
    end

    def common_classes_for_error
      self.class.common_classes.select do |klass, attrs|
        attrs[:methods].include? raised_exception.spell_checker.method_name
      end
    end


    @@string_methods  = String.new.methods
    @@integer_methods = 1.methods
    @@float_methods   = 1.0.methods
    @@array_methods   = Array.new.methods
    @@hash_methods    = Hash.new.methods

    def self.common_classes
      {
        String:  { methods: @@string_methods,  example: "Hello, world!" },
        Integer: { methods: @@integer_methods, example: 42 },
        Float:   { methods: @@float_methods,   example: 3.14 },
        Array:   { methods: @@array_methods,   example: '[]' },
        Hash:    { methods: @@hash_methods,    example: '{}' }
      }
    end

    def self.common_methods
      common_classes.map { |klass, attrs| attrs[:methods] }.flatten - Object.methods
    end

    private_class_method :common_methods

  end

end
