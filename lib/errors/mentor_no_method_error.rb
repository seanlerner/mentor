module Mentor

  class MentorNoMethodError < MentorError

    extend Outputs, Colorize

    def self.can_handle?
      Mentor.tp.raised_exception.class == NoMethodError
    end

    private

    def sections
      [
        Header.new,
        RubyErrorComplete.new,
        RelativePath.new,
        LinesOfCode.new
      ]
    end

  end

end
