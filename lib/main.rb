module Mentor

  class Main

    def initialize

      error_class = MentorError.find

      return unless error_class

      begin
        error = error_class.new
        raise error
      rescue *MentorError.error_classes
        error.output
        exit 1
      end

    end

  end

end
