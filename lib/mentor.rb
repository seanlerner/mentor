module Mentor

  class Init

    def self.setup
      require_relative 'helpers/globals'
      Mentor.extend(Globals)
      setup_trace_point
      Mentor.enable
    end

    def self.setup_trace_point

      TracePoint.trace(:raise) do |tp|
        tp.disable

        if Mentor.enabled?
          Mentor.disable
          require_relative 'helpers/requires'
          Mentor.tp = tp
          Main.new
        end

        tp.enable
      end

    end

    private_class_method :setup_trace_point

  end

end
