module Mentor

  class Docs

    include Outputs, Colorize

    attr_reader :lines

    def initialize(lines)
      @lines = ['For docs, type:', '']
      @lines << indent_lines(lines)
      @lines = indent_lines(@lines)
      colorize_section
    end

  end

end
