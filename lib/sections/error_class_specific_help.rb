module Mentor

  class ErrorClassSpecificHelp

    include Outputs, Colorize

    attr_reader :lines

    def initialize(lines)
      @lines = indent_lines(lines)
      colorize_section
    end

  end

end
