module Mentor

  class Header

    include Outputs, Colorize

    def lines
      @lines = [
        horizontal_line,
        title_indented,
        horizontal_line
      ]

      colorize_section
    end

    private

    def title_indented
      if title_bar_spacer_size > 4
        " #{ruby_error_text} #{ruby_error_class}#{title_bar_spacer_with_indent}#{file_name}:#{error_lineno}"
      elsif title_bar_spacer_size > 2
        "#{ruby_error_text} #{ruby_error_class}#{title_bar_spacer_without_indent}#{file_name}:#{error_lineno}"
      else
        "#{ruby_error_text} #{ruby_error_class}\n#{file_name}:#{error_lineno}"
      end
    end

    def title_bar_spacer_size
      start_of_line = ruby_error_text + ' ' + ruby_error_class
      end_of_line   = file_name + ':' + error_lineno
      terminal_width - (start_of_line.length + end_of_line.length)
    end

    def title_bar_spacer_without_indent
      ' ' * title_bar_spacer_size
    end

    def title_bar_spacer_with_indent
      ' ' * (title_bar_spacer_size - 2)
    end

  end

end
