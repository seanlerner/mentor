module Mentor

  class Suggestion

    include Outputs, Colorize

    attr_reader :lines

    def initialize(suggestion = '')
      @lines = suggestion
      format_lines
    end

    private

    def format_lines
      @lines = indent_lines(@lines)
      @lines.map! { |line| rainbow(line, :suggestion) }
    end

  end

end
