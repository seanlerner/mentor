module Mentor

  class Examples

    include Outputs, Colorize

    attr_reader :lines

    def initialize(examples)
      @lines =
        indent_lines([
          'For example:',
          '',
          examples
        ]).flatten

      examples.each do |example|
        @lines.map! do |line|
          line.sub(example, color_code(example))
        end
      end

      colorize_section
    end

  end

end
