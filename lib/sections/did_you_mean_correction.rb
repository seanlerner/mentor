module Mentor

  class DidYouMeanCorrection

    include Outputs

    attr_reader :line

    def initialize
      @line = "#{did_you_mean_text}  #{did_you_mean_word}"
    end

    def self.can_handle?
      Mentor.tp.raised_exception.respond_to?(:corrections) &&
        Mentor.tp.raised_exception.corrections.any?
    end

  end

end
