module Mentor

  class RubyErrorComplete

    include Outputs

    def lines
      lines_array = []
      lines_array << RubyErrorMain.new.lines
      lines_array << DidYouMeanCorrection.new.line if DidYouMeanCorrection.can_handle?
      lines_array << Backtrace.new.lines
      lines_array = indent_lines(lines_array)
      lines_array << ''
      lines_array << horizontal_line
    end

  end

end
