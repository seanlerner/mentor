module Mentor

  class LineOfCode < String

    include Outputs

    def initialize(lineno, line, max_length)
      @lineno     = lineno
      @line       = line
      @max_length = max_length
      super("#{padded_lineno}:  #{@line}")
    end

    private

    def padded_lineno
      if error_lineno?
        error_lineno_padded(@max_length)
      else
        lineno_subtle_padded(@max_length)
      end
    end

    def error_lineno?
      @lineno == Mentor.tp.lineno
    end

  end

end
