module Mentor

  class RubyErrorMain

    include Outputs

    def lines
      ruby_error_main
    end

    private

    def single_line
      [path_and_lineno, message, bracketed_class].flatten.join(' ')
    end

    def path_and_lineno
      app_path_and_lineno = "#{app_dir}#{file_name}:#{error_lineno}:in `#{calling_method}':"

      if backtrace_lines.any?
        absolute_base_dir + app_path_and_lineno
      else
        app_path_and_lineno
      end
    end

    def bracketed_class
      "(#{ruby_error_class})"
    end

    def ruby_error_main
      if single_line.size <= terminal_width
        [single_line]
      else
        ruby_error_wrapped
      end
    end

    def ruby_error_wrapped
      if fit_on_one_or_two_lines?(path_and_lineno, message)
        [path_and_lineno + ' ' + message, bracketed_class]

      elsif fit_on_one_or_two_lines?(message, bracketed_class)
        [path_and_lineno, message + ' ' + bracketed_class]

      else
        [path_and_lineno, message, bracketed_class]

      end
    end

    def fit_on_one_or_two_lines?(part_a, part_b)
      fit_on_one_line?(part_a, part_b) ||
        does_not_fit_on_one_line?(part_a) && fit_on_two_lines?(part_a, part_b)
    end

    def fit_on_one_line?(part_a, part_b)
      (part_a + ' ' + part_b).size <= terminal_width
    end

    def does_not_fit_on_one_line?(part)
      part.size >= terminal_width
    end

    def fit_on_two_lines?(part_a, part_b)
      (part_a + ' ' + part_b).size <= terminal_width * 2
    end

  end

end
