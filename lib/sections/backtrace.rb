module Mentor

  class Backtrace

    include Outputs

    def lines
      indent_lines(backtrace_lines, indent: 10)
    end

  end

end
