module Mentor

  class LinesOfCode

    include Outputs

    def lines
      indent_lines(lines_of_code_from_method_or_file, indent: 4)
    end

    private

    def lines_of_code_from_method_or_file
      if calling_method_can_be_obtained?
        set_first_and_last_from_method
      else
        set_first_and_last_from_file
      end

      lineno_max_length = "=> #{@last_lineno}".length

      (@first_lineno..@last_lineno).map do |lineno|
        LineOfCode.new(lineno, lines_from_file[lineno], lineno_max_length)
      end
    end

    def calling_method_can_be_obtained?
      calling_method != '<main>'
    end

    def set_first_and_last_from_method
      method_text = Pry::Method.from_str("#{Mentor.tp.defined_class}##{calling_method}").source.split("\n")
      @first_lineno = lines_from_file.select { |_lineno, line| line["def #{calling_method}"] }.sort.last.first
      @last_lineno  = @first_lineno + method_text.size - 1
    end

    def set_first_and_last_from_file
      @first_lineno = Mentor.tp.lineno
      @first_lineno -= 1 until first_line_at_start_of_file? ||
                               first_line_empty? ||
                               line_prior_to_first_line_ends?

      @last_lineno = Mentor.tp.lineno
      @last_lineno += 1 until last_line_at_end_of_file? ||
                              last_line_empty?
    end

    def first_line_at_start_of_file?
      @first_lineno == 1
    end

    def first_line_empty?
      lines_from_file[@first_lineno].empty?
    end

    def line_prior_to_first_line_ends?
      lines_from_file[@first_lineno - 1]['end']
    end

    def last_line_at_end_of_file?
      @last_lineno == lines_from_file.size
    end

    def last_line_empty?
      lines_from_file[@last_lineno].empty?
    end

  end

end
