module Mentor

  class RelativePath

    include Outputs

    def lines
      indent_lines(relative_base_dir + app_dir + file_name + ':' + error_lineno)
    end

  end

end
