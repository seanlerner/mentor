require 'minitest/autorun'
require 'minitest/pride'

class MentorTest < Minitest::Test

  def test_run_time
    ruby_output   = `ruby test/triggers/run_time.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/run_time.rb 2>&1`

    assert_match "RuntimeError",                           ruby_output
    assert_match "Ruby Error: RuntimeError",             mentor_output
    assert_match "run_time.rb:1",                        mentor_output
    assert_match "test/triggers/run_time.rb:1",          mentor_output
    assert_match ":in `<main>':",                        mentor_output
    assert_match "Custom Run Time Error Message",        mentor_output
    assert_match "(RuntimeError)",                       mentor_output
    assert_match "test/triggers/run_time.rb:1",          mentor_output
    assert_match "=> 1:",                                mentor_output
    assert_match "raise",                                mentor_output
    assert_match "Custom Run Time Error Message",        mentor_output
    refute_match "Did you mean?",                        mentor_output
    assert_match tidy(ruby_output), tidy(mentor_output), mentor_output
  end

  def test_no_method
    ruby_output   = `ruby test/triggers/no_method.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/no_method.rb 2>&1`

    assert_match "NoMethodError",                                    ruby_output
    assert_match "Ruby Error: NoMethodError",                      mentor_output
    assert_match "no_method.rb:1",                                 mentor_output
    assert_match "test/triggers/no_method.rb:1",                   mentor_output
    assert_match ":in `<main>':",                                  mentor_output
    assert_match "undefined method `bad_method' for Object:Class", mentor_output
    assert_match "(NoMethodError)",                                mentor_output
    assert_match "test/triggers/no_method.rb:1",                   mentor_output
    assert_match "=> 1: Object.bad_method",                   tidy(mentor_output)
    refute_match "Did you mean?",                                  mentor_output
    assert_match tidy(ruby_output), tidy(mentor_output),           mentor_output
  end

  def test_no_method_did_you_mean
    ruby_output   = `ruby test/triggers/no_method_did_you_mean.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/no_method_did_you_mean.rb 2>&1`

    assert_match "NoMethodError",                                           ruby_output
    assert_match "Ruby Error: NoMethodError",                             mentor_output
    assert_match "no_method_did_you_mean.rb:5",                           mentor_output
    assert_match "test/triggers/no_method_did_you_mean.rb:5",             mentor_output
    assert_match ":in `initialize':",                                     mentor_output
    assert_match "undefined method `sor' for []:Array",                   mentor_output
    assert_match "(NoMethodError)",                                       mentor_output
    assert_match "test/triggers/no_method_did_you_mean.rb:5",             mentor_output
    assert_match "3: def initialize",                                tidy(mentor_output)
    assert_match "=> 5: my_array.sor",                               tidy(mentor_output)
    assert_match "Did you mean? sort",                               tidy(mentor_output)
    assert_match "my_array does not have the method sor.",           tidy(mentor_output)
    assert_match "You may have made a typo.",                        tidy(mentor_output)
    assert_match "Try changing the method sor to sort on my_array.", tidy(mentor_output)
    assert_match tidy(ruby_output), tidy(mentor_output),                  mentor_output
  end

  def test_no_method_for_nil_class
    ruby_output   = `ruby test/triggers/no_method_for_nil_class.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/no_method_for_nil_class.rb 2>&1`

    assert_match "NoMethodError",                                                                              ruby_output
    assert_match "Ruby Error: NoMethodError",                                                                mentor_output
    assert_match "no_method_for_nil_class.rb:1",                                                             mentor_output
    assert_match "test/triggers/no_method_for_nil_class.rb:1",                                               mentor_output
    assert_match ":in `<main>':",                                                                            mentor_output
    assert_match "undefined method `bad_method' for nil:NilClass",                                           mentor_output
    assert_match "(NoMethodError)",                                                                          mentor_output
    assert_match "test/triggers/no_method_for_nil_class.rb:1",                                               mentor_output
    assert_match "=> 1: @not_defined.bad_method",                                                       tidy(mentor_output)
    assert_match "It looks like you're trying to call the method bad_method on @not_defined.",          tidy(mentor_output)
    assert_match "It's likely @not_defined was never defined, ",                                        tidy(mentor_output)
    assert_match "and therefore is equal to nil, which is how Ruby represents 'nothing'.",              tidy(mentor_output)
    assert_match "Try setting @not_defined to a value first and then call the method bad_method on it", tidy(mentor_output)
    refute_match "Did you mean?",                                                                            mentor_output
    assert_match tidy(ruby_output), tidy(mentor_output),                                                     mentor_output
  end

  def test_no_method_for_nil_class_sort_method
    ruby_output   = `ruby test/triggers/no_method_for_nil_class_sort_method.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/no_method_for_nil_class_sort_method.rb 2>&1`

    assert_match "NoMethodError",                                                                                 ruby_output
    assert_match "Ruby Error: NoMethodError",                                                                   mentor_output
    assert_match "no_method_for_nil_class_sort_method.rb:1",                                                    mentor_output
    assert_match "test/triggers/no_method_for_nil_class_sort_method.rb:1",                                      mentor_output
    assert_match ":in `<main>':",                                                                               mentor_output
    assert_match "undefined method `sort' for nil:NilClass",                                                    mentor_output
    assert_match "(NoMethodError)",                                                                             mentor_output
    assert_match "test/triggers/no_method_for_nil_class_sort_method.rb:1",                                      mentor_output
    assert_match "=> 1: @not_defined.sort",                                                                tidy(mentor_output)
    assert_match "It looks like you're trying to call the method sort on @not_defined.",                   tidy(mentor_output)
    assert_match "It's likely @not_defined was never defined, ",                                           tidy(mentor_output)
    assert_match "and therefore is equal to nil, which is how Ruby represents 'nothing'.",                 tidy(mentor_output)
    assert_match "Arrays and Hashes have the sort method,",                                                tidy(mentor_output)
    assert_match "so you may want to set @not_defined to an Array or Hash first.",                         tidy(mentor_output)
    assert_match "For example:",                                                                           tidy(mentor_output)
    assert_match "@not_defined = [] # sets it to an Array",                                                tidy(mentor_output)
    assert_match "@not_defined = {} # sets it to a Hash",                                                  tidy(mentor_output)
    assert_match "For docs, type:",                                                                        tidy(mentor_output)
    assert_match "ri Array#sort",                                                                          tidy(mentor_output)
    assert_match "ri Hash#sort",                                                                           tidy(mentor_output)
    assert_match "Try setting @not_defined to an Array or Hash first and then call the method sort on it", tidy(mentor_output)
    refute_match "Did you mean?",                                                                               mentor_output
    assert_match tidy(ruby_output), tidy(mentor_output),                                                        mentor_output
  end

  def test_multi_file
    ruby_output   = `ruby test/triggers/multi_file_trigger_part_1.rb 2>&1`
    mentor_output = `#{Dir.pwd}/bin/mentor test/triggers/multi_file_trigger_part_1.rb 2>&1`

    assert_match "NoMethodError",                                                   ruby_output
    assert_match "Ruby Error: NoMethodError",                                     mentor_output
    assert_match "multi_file_trigger_part_2.rb:4",                                mentor_output
    assert_match "test/triggers/multi_file_trigger_part_2.rb:4",                  mentor_output
    assert_match "test/triggers/multi_file_trigger_part_2.rb:4",                  mentor_output
    assert_match ":in `initialize':",                                             mentor_output
    assert_match "undefined method `bad_method' for 1:Integer",                   mentor_output
    assert_match "(NoMethodError)",                                               mentor_output
    assert_match "from test/triggers/multi_file_trigger_part_1.rb:5:in `new'",    mentor_output
    assert_match "from test/triggers/multi_file_trigger_part_1.rb:5:in `<main>'", mentor_output
    assert_match "test/triggers/multi_file_trigger_part_2.rb:4",                  mentor_output
    assert_match "3: def initialize(my_integer)",                            tidy(mentor_output)
    assert_match "=> 4: my_integer.bad_method",                              tidy(mentor_output)
    assert_match "5: end",                                                   tidy(mentor_output)
    assert_match tidy(ruby_output), tidy(mentor_output),                          mentor_output
  end

  def test_usage_message
    usage_message = `#{Dir.pwd}/bin/mentor`
    assert_match "Usage:",                      usage_message
    assert_match "mentor your_ruby_program.rb", usage_message
  end

  def test_from_home_dir
    old_dir = Dir.pwd
    Dir.chdir
    output = `#{old_dir}/bin/mentor #{old_dir}/test/triggers/multi_file_trigger_part_1.rb 2>&1`
    assert_match "Ruby Error: NoMethodError", output
    Dir.chdir old_dir
  end

  def test_from_desktop
    old_dir = Dir.pwd
    Dir.chdir
    Dir.chdir 'Desktop'
    output = `#{old_dir}/bin/mentor #{old_dir}/test/triggers/multi_file_trigger_part_1.rb 2>&1`
    assert_match "Ruby Error: NoMethodError", output, output
    Dir.chdir old_dir
  end

  private

  def tidy(output)
    output
      .gsub(/\e.*?m/, '') # remove_colors
      .tr("\n", ' ')      # remove_linefeeds
      .tr("\t", ' ')      # remove_tabs
      .squeeze(' ')       # remove_double_spaces
  end

end
