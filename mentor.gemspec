lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mentor/version'

Gem::Specification.new do |spec|
  spec.name     = 'mentor'
  spec.version  = Mentor::VERSION
  spec.authors  = ['Sean Lerner']
  spec.email    = ['sean@smallcity.ca']
  spec.summary  = 'Helpful error messages for new ruby programmers.'
  spec.homepage = 'https://gitlab.com/seanlerner/mentor'
  spec.license  = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.executables << 'mentor'
  spec.require_paths = ['lib']

  spec.add_dependency 'pry',     '~> 0.10'
  spec.add_dependency 'rainbow', '~> 2.2'
  spec.add_dependency 'rouge',   '~> 2.0'

  spec.add_development_dependency 'bundler',        '~> 1.13'
  spec.add_development_dependency 'did_you_mean',   '~> 1.1'
  spec.add_development_dependency 'guard',          '~> 2.14'
  spec.add_development_dependency 'guard-minitest', '~> 2.4'
  spec.add_development_dependency 'minitest',       '~> 5.0'
  spec.add_development_dependency 'rake',           '~> 10.0'
end
