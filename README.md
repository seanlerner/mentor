# Mentor

Mentor is a command line application for new Ruby developers. It gives descriptive error messages and where possible provides suggestions on how to best fix errors.

Instead of using Ruby to run your ruby programs like so:

```
ruby my_ruby_program.rb
```

Use Mentor:

```
mentor my_ruby_program.rb
```

## Installation

From your command line, type:

```
gem install mentor
```

## Usage

From your command line, type:

```
mentor my_ruby_program.rb
```

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/seanlerner/mentor.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Credits

Mentor was created by Sean Lerner.
